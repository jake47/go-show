<?php
/**
 * Created by PhpStorm.
 * User: ZHANDOS
 * Date: 24.10.2018
 * Time: 21:22
 */

namespace app\models;

use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'product';
    }

    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}