<?php
/**
 * Created by PhpStorm.
 * User: ZHANDOS
 * Date: 24.10.2018
 * Time: 21:22
 */

namespace app\models;

use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public static function tableName()
    {
        return 'category';
    }

    public function getProducts() {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
}